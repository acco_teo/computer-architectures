/*-------------------------------------------------------------------------------------------------
 * Name:    sample.c
 * Purpose: Extra points project
 * Note(s): Pedestrian and car semaphore simulation
 *-------------------------------------------------------------------------------------------------
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2017 Politecnico di Torino. All rights reserved.
 *-------------------------------------------------------------------------------------------------	*/

#include <stdio.h>
#include "LPC17xx.H"										/* LPC17xx definitions																			*/
#include "led/led.h"
#include "button_EXINT/button.h"
#include "timer/timer.h"

extern unsigned char led_value;					/* defined in funct_led																			*/
extern unsigned int timer0_value;				/* defined in lib_timer																			*/
extern unsigned int buttonPressed;			/* defined in lib_button				 														*/

/*-------------------------------------------------------------------------------------------------
  Main Program
	- Button Interrupts and Timer Interrupts have the same priority, in order to avoid nested
		interrupt handlers calls. Grant of sequential execution when nedeed.
	- LPC_TIM0->TC != 0 checks if TIMER0 is not over.
	- Protection policy during state switches and IRQ handlers execution:
		Where NOT needed IRQ pending are cleared, according to a FIFO policy.
			eg. Button click on (Green Pedestrian, Red Car), during the execution of the handler
			TIMER0 expires -> TIMER0 resetted, IRQ_TIMER0 cleared
 *------------------------------------------------------------------------------------------------- */
int main (void) {

	SystemInit();  													/* System Initialization (i.e., PLL)											*/
  LED_init();															/* LED Initialization																			*/
  BUTTON_init();													/* BUTTON Initialization																	*/

	LPC_SC->PCON |= 0x1;										/* Power-Down	mode																				*/
	LPC_SC->PCON &= 0xFFFFFFFFD;

	while(1){
		LED_Out(0x60);												/* (Green Pedestrian, Red Car)														*/
		init_timer(0, 0x0EE6B280);						/* TIMER0 Initialization, 15" (1" = 0x017D7840)						*/
		enable_timer(0);											/* TIMER0 enable 																					*/
		buttonPressed = 0;										/* Clear/init buttonPressed from previous cycles 					*/
		while(LPC_TIM0->TC != 0){ 						/* Sleep until timer is over. The timer can be resetted		*/
			__ASM("wfi");												/* if a button is pressed at this stage 									*/
		}																			/* wait for timerINT or buttonINT 												*/

		init_timer(0, 0x07735940);						/* TIMER0 Initialization, 5" 															*/
		init_timer(1, 0x00BEBC20);						/* TIMER1 Initialization, 0.5" 														*/
		enable_timer(0);											/* TIMER0 enable																					*/
		enable_timer(1);											/* TIMER1 enable 																					*/

		do {																	/* (Flashing Green Pedestrian, Red Car) 									*/
			LED_Out(0x20);
			__ASM("wfi");
			if(buttonPressed){									/* An IRQbutton raised, exit from while cycle immediatly 	*/
				break;
			}
			LED_Out(0x60);											/* Not needed the break, the state is already green for  	*/
			__ASM("wfi");												/* pedestrian, just exit from the while cycle 					 	*/
		}
		while((LPC_TIM0->TC != 0) && (!buttonPressed));

		if(buttonPressed){										/* An IRQbutton is detected, restart the while(1) cycle 	*/
			continue;
		}

		/* Until a request for green for pedestrian is raised wait in (Red Pedestrian, Green Car) 			*/
		/* At this point there aren't active timers, only a IRQbutton makes the program continue  			*/
		while(led_value == 0x88){
			__ASM("wfi");
		}

		/* Let the time flow until the state is (Red Pedestrian, Yellow Car) and timer is over 					*/
		while((led_value == 0x90) && (LPC_TIM0->TC != 0)){
			__ASM("wfi");
		}

  }

}
