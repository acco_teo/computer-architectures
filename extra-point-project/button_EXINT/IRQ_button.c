#include "button.h"
#include "lpc17xx.h"
#include "../timer/timer.h"
#include "../led/led.h"

extern unsigned char led_value;			/* defined in funct_led					*/
extern unsigned int timer0_value;		/* defined in lib_timer					*/
extern unsigned int buttonPressed;	/* 0 = not pressed, 1 = pressed */
																		/* shared between KEY1 and KEY2 */

void EINT1_IRQHandler (void)	  	/* KEY1	*/
{
	if(led_value == 0x60 && timer0_value == 0x023C3460)
	{
		/* A button is pressed at the initial state, just reset 15 seconds timer 																*/
		reset_timer(0);
		init_timer(0, 0x023C3460);
		enable_timer(0);
		NVIC_ClearPendingIRQ(TIMER0_IRQn);				/* TIMER0 expired during IRQbutton handler execution. 				*/
																							/* Pressed before timeout, next state is determined by button */
	}
	else if(((led_value == 0x60)||(led_value == 0x20))&&(timer0_value == 0x00BEBC20))
	{
		/* A button is pressed during (Flashing Green Pedestrian, Red Car), back to initial state 							*/
		disable_timer(1);													/* disable TIMER1 																						*/
		reset_timer(1);														/* clear TIMER1																								*/
		buttonPressed = 1;
		NVIC_ClearPendingIRQ(TIMER0_IRQn);				/* TIMER0 expired during IRQbutton handler execution. 				*/
																							/* Pressed before timeout, next state is determined by button */
	}
	else if(led_value == 0x88)
	{
		if(!buttonPressed)												/* buttonPressed is used to avoid multiple clicks of a button */
		{																					/* once is pressed successive clicks are ignored 							*/
			enable_timer(0);
			buttonPressed = 1;
		}																					/* Here TIMER0 interrupt request must not be ingored! 				*/
	}
	LPC_SC->EXTINT &= (1 << 1);									/* clear pending interrupt																		*/
}

void EINT2_IRQHandler (void)	  	/* KEY2	*/
{
	if(led_value == 0x60 && timer0_value == 0x023C3460)
	{
		/* A button is pressed at the initial state, just reset 15 seconds timer 																*/
		reset_timer(0);
		init_timer(0, 0x023C3460);
		enable_timer(0);
		NVIC_ClearPendingIRQ(TIMER0_IRQn);				/* TIMER0 expired during IRQbutton handler execution. 				*/
																							/* Pressed before timeout, next state is determined by button */
	}
	else if(((led_value == 0x60)||(led_value == 0x20))&&(timer0_value == 0x00BEBC20))
	{
		/* A button is pressed during (Flashing Green Pedestrian, Red Car), back to initial state 							*/
		disable_timer(1);													/* disable TIMER1 																						*/
		reset_timer(1);														/* clear TIMER1																								*/
		buttonPressed = 1;
		NVIC_ClearPendingIRQ(TIMER0_IRQn);
	}
	else if(led_value == 0x88)
	{
		if(!buttonPressed)												/* buttonPressed is used to avoid multiple clicks of a button */
		{																					/* once is pressed successive clicks are ignored 							*/
			enable_timer(0);
			buttonPressed = 1;
		}																					/* Here TIMER0 interrupt request must not be ingored! 				*/
	}
  LPC_SC->EXTINT &= (1 << 2);									/* clear pending interrupt																		*/
}
