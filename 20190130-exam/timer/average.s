				AREA asm_functions, CODE, READONLY				
                EXPORT  average
average
				; save current SP for a faster access 
				; to parameters in the stack
				MOV   R12, SP
				; save volatile registers
				STMFD SP!, {R4-R8, R10-R11, LR}				
				
				MOV R5, #0			; clear sum register
				MOV R4, R1			; move N to R4
next			LDR R6, [R0]		; move V[R0] to R6
				ADDS R5, R5, R6		; add with flags modification
				BVS ex				; brach if overflow
				BCS ex				; branch if carry
				BMI ex				; branch if negative
				ADD R0, R0, #4		; switch to next element
				SUBS R4, R4, #1		; decrement cycle counter
				BNE next
				
				UDIV R0, R5, R1		; R4 <- sum / N
				B exit				; jump to end

ex				MOV R0, #0			; end with exceptions
				
				; restore volatile registers
exit			LDMFD SP!, {R4-R8, R10-R11, PC}
				
                END