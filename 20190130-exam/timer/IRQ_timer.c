/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_timer.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    timer.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "lpc17xx.h"
#include "timer.h"
#include "../led/led.h"
#include "../RIT/RIT.h"

#define N 5

/******************************************************************************
** Function name:		Timer0_IRQHandler
**
** Descriptions:		Timer/Counter 0 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/

extern int var1;																	/* defined in IRQ_RIT.c */
extern int average(int v[N], int n);

void TIMER0_IRQHandler (void)
{
	static int i = 0;
	static int v[N]={0};
	int func_value;

	v[i] = var1;
	i++;
	if(i == N){																			/* vector is full 															*/
		LED_Out(0);																		/* clear led state, safety operation 						*/
		disable_timer(1);															/* disable flashing led timer, safety operation */
		reset_timer(1);
		i = 0;																				/* clear index																	*/
		func_value = average(v, N);										/* calling ASM function 												*/
		if(func_value == 0){									
				LED_On(7);
		}
		else if(func_value <= 63){
				LED_Out((func_value) & 0x3F); 							
		}
		else{
			enable_timer(1);
		}												
	}
	var1 = 0;
  LPC_TIM0->IR = 1;			/* clear interrupt flag */
  return;
}


/******************************************************************************
** Function name:		Timer1_IRQHandler
**
** Descriptions:		Timer/Counter 1 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
extern unsigned char led_value;
void TIMER1_IRQHandler (void)
{
	if(led_value == 0x00)	/* reading led state through led_value vair*/
		LED_On(6);
	else
		LED_Off(6);
  LPC_TIM1->IR = 1;			/* clear interrupt flag */
  return;
}


