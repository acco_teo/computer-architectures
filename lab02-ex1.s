;******** Lab 2 - Ex 1 ********
; arithmetic operations between
; floating point stored in four
; vectors
; v5[i] = v1[i]*v2[i];
; v6[i] = v2[i]/v3[i];
; v7[i] = v1[i]+v4[i];

; ************** Register Plan ***************
; - R1 -> V1 displacement		 - F1 -> V1[i]
; - R2 -> V2 displacement		 - F2 -> V2[i]
; - R3 -> V3 displacement		 - F3 -> V3[i]
; - R4 -> V4 displacement		 - F4 -> V4[i]
; - R5 -> V5 displacement		 - F5 -> V5[i]
; - R6 -> V6 displacement		 - F6 -> V6[i]
; - R7 -> V7 displacement		 - F7 -> V7[i]
; - R8 -> cycles number

.data
V1: .double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
    .double 54, 53, 67, 32, 45, 32, 23, 65, 12, 65
    .double 78, 98, 54, 67, 24, 82, 25, 21, 45, 63
V2: .double 32, 52, 1, 42, 45, 67, 65, 35, 86, 13
    .double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
    .double 76, 42, 24, 76, 92, 82, 13, 71, 72, 33
V3: .double 45, 7, 50, 6, 15, 77, 5, 35, 4, 77
    .double 32, 52, 1, 42, 45, 67, 65, 35, 86, 13
    .double 64, 71, 45, 13, 9, 11, 74, 62, 87, 31
V4: .double 54, 53, 67, 32, 45, 32, 23, 65, 12, 65
    .double 45, 7, 50, 6, 15, 77, 5, 35, 4, 77
    .double 17, 61, 13, 74, 54, 72, 8, 44, 74, 9
V5: .space 240
V6: .space 240
V7: .space 240

.text
main: daddui	R8, R0, 30		; setting up vectors displacements
      daddui 	R1, R0, V1
      daddui	R2, R0, V2
      daddui	R3, R0, V3
      daddui	R4, R0, V4
      daddui	R5, R0, V5
      daddui	R6, R0, V6
      daddui	R7, R0, V7

      l.d	    F2, 0(R2)
      l.d	    F3, 0(R3)
      l.d	    F1, 0(R1)
      l.d	    F4, 0(R4)

loop: div.d		F6, F2, F3		; sort of do-while approach, trying to anticipate long instruction
      mul.d		F5, F1, F2		; such as div and mul in order to pipeline registers increments,
      add.d		F7, F1, F4		; loads and stores

      daddi		R8, R8, -1		; decrement cycle counter

      daddi		R1, R1, 8		  ; increment V1, V2, V3, V4 displacements
      daddi		R4, R4, 8
      daddi		R3 ,R3, 8
      daddi 	R2, R2, 8

      l.d	    F1, 0(R1)		  ; load next elements
      l.d	    F4, 0(R4)
      l.d	    F2, 0(R2)
      l.d     F3, 0(R3)

      s.d	    F7, 0(R7)     ; store results
      s.d     F5, 0(R5)
      s.d     F6, 0(R6)

      daddi 	R7, R7, 8     ; increment V5, V6, V7 displacements
      daddi 	R5, R5, 8
      daddi 	R6, R6, 8

      bnez 		R8, loop

      halt
