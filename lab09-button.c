#include "button.h"
#include "lpc17xx.h"

#include "../led/led.h"

int var;

void EINT0_IRQHandler (void)	  	/* INT0														 */
{
	var = 0;
	LED_Out(0x01);
	LPC_SC->EXTINT &= (1 << 0);     /* clear pending interrupt         */
}


void EINT1_IRQHandler (void)	  	/* KEY1														 */
{

	if(var == 7)
		var = 0;
	else
		var++;
  LED_Out(0);
	LED_On(var);
	LPC_SC->EXTINT &= (1 << 1);     /* clear pending interrupt         */
}

void EINT2_IRQHandler (void)	  	/* KEY2														 */
{
if(var == 0)
		var = 7;
else
		var--;
  LED_Out(0);
	LED_On(var);
  LPC_SC->EXTINT &= (1 << 2);     /* clear pending interrupt         */
}
