;********** Lab 3 - Ex 1 **********
; perform optimizations on lab 2
; ex 1  changing operations order
; with forward enabled
; v3[i] = v1[i]*v2[i];
; v4[i] = v3[i]/v2[i];
; v5[i] = v4[i]+v2[i];

; ************** Register Plan ***************
; - R1 -> V1 displacement		 - F1 -> V1[i]
; - R2 -> V2 displacement		 - F2 -> V2[i]
; - R3 -> V3 displacement		 - F3 -> V3[i]
; - R4 -> V4 displacement		 - F4 -> V4[i]
; - R5 -> V5 displacement		 - F5 -> V5[i]
; - R8 -> cycles number

.data
V1: .double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
	.double 54, 53, 67, 32, 45, 32, 23, 65, 12, 65
	.double 78, 98, 54, 67, 24, 82, 25, 21, 45, 63
V2: .double 32, 52, 1, 42, 45, 67, 65, 35, 86, 13
	.double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
	.double 76, 42, 24, 76, 92, 82, 13, 71, 72, 33
V3: .space 240
V4: .space 240
V5: .space 240

.text
main:   daddui	R8, R0, 30		; setting up vectors displacements
        daddui 	R1, R0, V1
		daddui	R2, R0, V2
		l.d		F1, 0(R1)
		l.d		F2, 0(R2)
		daddui	R3, R0, V3
		daddui	R4, R0, V4
		daddui	R5, R0, V5

loop:   mul.d	F3, F1, F2
		daddi	R1, R1, 8
		daddi	R2, R2, 8
		daddi	R8, R8, -1		; decrement cycle counter

		div.d	F4, F3, F2
		add.d	F5, F4, F2

		l.d		F1, 0(R1)

		s.d		F3, 0(R3)		; store mul result
		daddi 	R3, R3, 8		; increment V3 displacement

		s.d		F4, 0(R4)		; store div and add result
		daddi 	R4, R4, 8		; increment V4, V5 displacements

		l.d		F2, 0(R2)
		s.d		F5, 0(R5)
		daddi 	R5, R5, 8

		bnez 		R8, loop

		halt
