;******** Lab 2 - Ex 2 ********
; compute the bit parity value
; for a 100 elements vector
; 8 bit -> 0-6 data
;		-> 7 parity bit
; odd  -> MSB=1
; even -> MSB=0
; (2^7)-1 = 127 max value

; ************** Register Plan ***************
; - R1 -> cycles numnber
; - R2 -> X displacement
; - R3 -> not used
; - R4 -> X[i]
; - R5 -> shifted bit
; - R6 -> parity bit
; - R7 -> odd mask

.data
X:  .byte 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
    .byte 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
    .byte 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48
    .byte 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64
    .byte 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80
    .byte 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96
    .byte 97, 98, 99, 100
; 16 values for row -> memory alignment

.text
main:   daddui 	R1, R0, 100
        daddui 	R2, R0, X
        daddui	R7, R0, 128     ; save odd mask

loop:   lb      R4, 0(R2)
        daddu	  R6, R0, R0      ; clean previous parity bit

        dsrl 	  R5, R4, 4       ; right shift 4 bit
        xor     R6, R4, R5      ; XOR with not shifted value
        dsrl    R5, R6, 2       ; right shift 2 bit
        xor     R6, R6, R5      ; XOR whit previous value
        dsrl    R5, R6, 1       ; right shift 1 bit
        xor     R6, R6, R5      ; XOR whit previous value
        andi    R6, R6, 1       ; selection of the LSB of XOR cahined operation

        daddi	  R1, R1, -1      ; avoiding RAW stall

        beqz 	  R6, end_c       ; if(parity_bit == 0) then jump to end_c

        or      R4,	R4, R7      ; parity_bit = 1 -> setting using odd mask
        sb      R4, 0(R2)       ; store again in X[i]

end_c:  daddi 	R2, R2, 1
        bnez    R1, loop

        halt
