/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_RIT.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    RIT.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "lpc17xx.h"
#include "RIT.h"
#include "../led/led.h"
#include "../timer/timer.h"

/******************************************************************************
** Function name:		RIT_IRQHandler
**
** Descriptions:		REPETITIVE INTERRUPT TIMER handler
**
** parameters:			None
** Returned value:	None
**
******************************************************************************/

int down_INT0 = 0;	
int down_KEY1 = 0;
int down_KEY2 = 0;

int var1 = 0;

void RIT_IRQHandler (void)
{		
		/* ----------- debouncing INT0 ----------- */
		if(down_INT0 != 0){
			if((LPC_GPIO2->FIOPIN & (1<<10)) == 0){
				if(down_INT0 == 1)
						var1 += 2;
				down_INT0++;
			}
			else {	/* button released */
				down_INT0=0;			
				disable_RIT();
				reset_RIT();
				NVIC_EnableIRQ(EINT0_IRQn);							 /* disable Button interrupts			*/
				LPC_PINCON->PINSEL4    |= (1 << 20);     /* External interrupt 0 pin selection */
		}
	}
	
	/* ----------- debouncing KEY1 ----------- */
	if(down_KEY1 != 0 ){
		if((LPC_GPIO2->FIOPIN & (1<<11)) == 0){
			if(down_KEY1 == 1)
				var1 = 0;
			down_KEY1++;
		}
		else {																		/* button released */
			down_KEY1=0;			
			disable_RIT();
			reset_RIT();
			NVIC_EnableIRQ(EINT1_IRQn);							/* disable Button interrupts			*/
			LPC_PINCON->PINSEL4 |= (1 << 22);				/* External interrupt 0 pin selection */
		}
	}
	
	
	/* ----------- debouncing KEY2 ----------- */
	if(down_KEY2 != 0 ){
		if((LPC_GPIO2->FIOPIN & (1<<12)) == 0){
			if(down_KEY2 == 1)
				var1 += 8;
			down_KEY2++;
		}
		else {																		/* button released */
			down_KEY2=0;			
			disable_RIT();
			reset_RIT();
			NVIC_EnableIRQ(EINT2_IRQn);							/* disable Button interrupts			*/
			LPC_PINCON->PINSEL4 |= (1 << 24);				/* External interrupt 0 pin selection */
		}
	}
	
  LPC_RIT->RICTRL |= 0x1;										/* clear interrupt flag */
	
  return;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
