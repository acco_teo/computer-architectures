;********** Lab 3 - Ex 1 **********
; perform optimizations on lab 2
; ex 1  loop unrolling

; v3[i] = v1[i]*v2[i];
; v4[i] = v3[i]/v2[i];
; v5[i] = v4[i]+v2[i];

; ************** Register Plan ***************
; - R1 -> V1 displacement
; - R2 -> V2 displacement
; - R3 -> V3 displacement
; - R4 -> V4 displacement
; - R5 -> V5 displacement
; - F1-F5 	-> first iteration values
; - F11-F15 -> second iteration values
; - F21-F25 -> third iteration values
; - R8 -> cycles number

.data
V1: .double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
		.double 54, 53, 67, 32, 45, 32, 23, 65, 12, 65
		.double 78, 98, 54, 67, 24, 82, 25, 21, 45, 63
V2: .double 32, 52, 1, 42, 45, 67, 65, 35, 86, 13
		.double 50, 23, 90, 30, 23, 12, 87, 15, 46, 57
		.double 76, 42, 24, 76, 92, 82, 13, 71, 72, 33
V3: .space 240
V4: .space 240
V5: .space 240

.text
main: daddui	R8, R0, 10		; cycles 30/3 unfolds -> 10
			daddui 	R1, R0, V1 		; setting up vectors displacements
			daddui	R2, R0, V2
			daddui	R3, R0, V3
			daddui	R4, R0, V4
			daddui	R5, R0, V5

loop: l.d			F1, 0(R1)			; first iteration load
			l.d			F2, 0(R2)
			daddi		R1, R1, 8
			mul.d		F3, F1, F2 		; first iteration mul
			daddi		R2, R2, 8

			l.d			F11, 0(R1)		; second iteration load
			l.d			F12, 0(R2)
			daddi		R1, R1, 8
			daddi		R2, R2, 8

			daddi		R8, R8, -1		; decrement cycle counter

			mul.d		F13, F11, F12 ; second iteration mul
			div.d		F4, F3, F2 		; first iteration div

			l.d			F21, 0(R1)		; third iteration load
			l.d			F22, 0(R2)
			daddi		R1, R1, 8
			daddi		R2, R2, 8

			mul.d		F23, F21, F22 ; third iteration mul
			div.d		F14, F13, F12 ; second iteration div
			add.d		F5, F4, F2 		; first iteration add

			s.d			F3, 0(R3)			; store first iteration results
			daddi 	R3, R3, 8
			s.d			F4, 0(R4)
			daddi 	R4, R4, 8
			s.d			F5, 0(R5)
			daddi 	R5, R5, 8

			div.d		F24, F23, F22 ; third iteration div
			add.d		F15, F14, F12 ; second iteration add

			s.d			F13, 0(R3)		; store second iteration results
			daddi 	R3, R3, 8
			s.d			F14, 0(R4)
			daddi 	R4, R4, 8
			s.d			F15, 0(R5)
			daddi 	R5, R5, 8

			s.d			F23, 0(R3)		; store third iteration mul
			daddi 	R3, R3, 8

			add.d		F25, F24, F22 ; third iteration add

			s.d			F24, 0(R4)		; store third iteration div
			daddi 	R4, R4, 8
			s.d			F25, 0(R5)		; store third iteration add

			bnez 		R8, loop
			daddi 	R5, R5, 8		; delay slot enabled

			halt
