/*********************************************************************************************************
**--------------File Info---------------------------------------------------------------------------------
** File name:           IRQ_timer.c
** Last modified Date:  2014-09-25
** Last Version:        V1.00
** Descriptions:        functions to manage T0 and T1 interrupts
** Correlated files:    timer.h
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#include "lpc17xx.h"
#include "timer.h"
#include "../led/led.h"

extern unsigned char led_value;						/* defined in funct_led					*/
extern unsigned int timer0_value; 				/* defined in lib_timer					*/

/******************************************************************************
** Function name:		Timer0_IRQHandler
**
** Descriptions:		Timer/Counter 0 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/

void TIMER0_IRQHandler (void)
{
	/* End of (Flashing Green Pedestrian, Red Car) */
	if(((led_value == 0x60)||(led_value == 0x20))&&(timer0_value == 0x00BEBC20))
	{
		LED_Out(0x88);													/* (Red Pedestrian, Green Car)										*/
		disable_timer(1);												/* disable TIMER1 used for flashing led 					*/
		reset_timer(1);													/* clear TIMER1 TC used for flashing led					*/
	}
	/* End of (Red Pedestrian, Green Car)																												*/
	else if(led_value == 0x88)
	{
		LED_Out(0x90);													/* (Red Pedestrian, Yellow Car) 									*/
		enable_timer(0);												/* TIMER0 enable 																	*/
	}
	LPC_TIM0->IR = 1;													/* clear interrupt flag 													*/
	return;
}

/******************************************************************************
** Function name:		Timer1_IRQHandler
**
** Descriptions:		Timer/Counter 1 interrupt handler
**
** parameters:			None
** Returned value:		None
**
******************************************************************************/
void TIMER1_IRQHandler (void)
{
	/* dummy handler, just used for flashing led */
  LPC_TIM1->IR = 1;			/* clear interrupt flag */
  return;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
