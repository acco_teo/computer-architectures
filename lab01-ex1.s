;******* Lab 1 - Ex 1 *******
; Maximum integer number in a
; vector of 100 signed values

; ******* Register Plan *******
; - R1 -> load V element
; - R2 -> displacement, vector index
; - R3 -> end condition (8 bytes * 100 element = 800)
; - R4 -> partial maximum
; - R5 -> set on less than (slt) flag

.data
V:   .word -10, 15, 10, -55, 1, -99, 43, 11, -9, 14
     .word 89, 9, 22, 6, 95, -99, 3, 10, 33, 44
     .word 11, -23, 55, 56, 74, 29, 18, -89, 0, 33
     .word 32, 18, 25, 61, 43, 25, -65, -91, 44, 29
     .word 1, -52, -71, -56, -42, 58, 29, 32, -83, -12
     .word -44, 21, 31, 54, 99, 54, 1, 9, 89, 10
     .word 52, 72, 31, -64, -87, 21, -54, -76, 25, -53
     .word 3, 32, -53, -68, -46, -23, 13, 43, 12, -78
     .word 23, -43, 23, 12, -65, 79, 12, 36, 63, -45
     .word -32, 53, 36, 64, -32, -1, -14, 23, 7, -49
max: .space 8

.text
main:   daddui r2, r0, 0
        daddui r3, r0, 800
        daddui r4, r0, 0
        ld     r4, V(r2) 		; assuming first V element as max
        daddui r2, r2, 8		; moving to next V element

cycle:  ld     r1, V(r2)
        daddui r2, r2, 8		; moving to next V element
        slt    r5, r4, r1 		; if(r4 < r1) r5=1
        bnez   r5, update		; if r5==1 then jump to update in order to update maximum value
;  	    nop

back:   bne    r2, r3, cycle	; moving to next V element
;	    nop
        j      end
;       nop

update: daddu  r4, r0, r1 	    ; update maximum value
        j      back
;       nop

end:    sd     r4, max(r0)
        halt
